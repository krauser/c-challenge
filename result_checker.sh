#!/bin/bash
## \file result_checker.sh
## 
## \author Jean-Baptiste Laurent
## \<jeanbaptiste.laurent.pro@gmail.com\>
## 
## \date Started on  Sat Sep 27 23:57:05 2014 Jean-Baptiste Laurent
## \date Last update Sun Sep 28 00:43:57 2014 Jean-Baptiste Laurent
## \brief This script check that the programme get the right result
##

bin="./challenge"
medium_log="output_challenge_medium.txt"
hard_log="output_challenge_hard.txt"

echo "Rebuilding code ..."
make fclean
make re
if [ -x $bin ] ; then
    echo "Starting $bin"
    $($bin 2> /dev/null)
    echo "End. Checking result ..."
else
    echo "Build failed ..."
fi

if [ -f $medium_log ] ; then
    if [ -f "reference_$medium_log" ] ; then
	res=$(diff "$medium_log" "reference_$medium_log")
	if [ -z "$res" ] ; then
	    echo "Congratulation: Medium challenge is working ! "
	else
	    echo "Failure: A diff occured. Output:"
	    echo "$res"
	fi
    else
	echo "Error: The reference output of the medium test is missing !"
    fi
else
    echo "Error: Missing $medium_log"
fi

if [ -f $hard_log ] ; then
    if [ -f "reference_$hard_log" ] ; then
	res=$(diff "$hard_log" "reference_$hard_log")
	if [ -z "$res" ] ; then
	    echo "Congratulation: hard challenge is working ! "
	else
	    echo "Failure: A diff occured. Output:"
	    echo $res
	fi
    else
	echo "Error: The reference output of the hard test is missing !"
    fi
else
    echo "Error: Missing $hard_log."
fi
