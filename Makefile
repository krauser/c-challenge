##
## \file Makefile
## 
## \author Jean-Baptiste Laurent
## \<jeanbaptiste.laurent.pro@gmail.com\>
## 
## \date Started on  Sat Sep 27 22:55:43 2014 Jean-Baptiste Laurent
## \date Last update Sun Sep 28 00:08:23 2014 Jean-Baptiste Laurent
## \brief Simple Makefile. You are only allowed to add flags to the CFLAGS.
##

NAME = challenge

CC = gcc

CFLAGS = -O2 -W -Wall -Wextra -Werror -Wno-empty-body	# Mandatory flags
CFLAGS +=						# Add your flags here

SRC = 	challenge.c	\
	hidden_main.c

LOG =	output_challenge_medium.txt	\
	output_challenge_hard.txt

OBJS = $(SRC:.c=.o)

all: $(NAME)

$(NAME): $(OBJS)
	$(CC) $(CFLAGS) $(OBJS) -o $(NAME)

clean:
	$(RM) $(OBJS) $(LOG)

fclean: clean
	$(RM) $(NAME)

re: fclean all

.PHONY: all clean fclean re
