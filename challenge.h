/*!
** \file challenge.h
**
** \author Jean-Baptiste Laurent
** \<jeanbaptiste.laurent.pro@gmail.com\>
**
** \date Started on  Sat Sep 27 23:26:18 2014 Jean-Baptiste Laurent
** \date Last update Wed Oct  8 15:40:28 2014 Jean-Baptiste Laurent
** \brief Header
*/

#ifndef CHALLENGE_HEADER_H_
# define CHALLENGE_HEADER_H_

# include <stdio.h>	/* FILE *, fopen(), fprintf() */

/* Common function */
int	my_calc(char op, int a, int b);

/* Challenge 1 */
int	get_result_super_easy(char seed);

/* Challenge 2 */
int	get_result_easy(int seed, char const *base);

/* Challenge 3 */
int	get_result_medium(FILE *fd, int const seed);

/* Challenge 4 */
ssize_t	get_result_hard(FILE *fd, int const seed);

#endif /* !CHALLENGE_HEADER_H_ */
