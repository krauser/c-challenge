/*!
** \file challenge.c
**
** \author Jean-Baptiste Laurent
** \<jeanbaptiste.laurent.pro@gmail.com\>
**
** \date Started on  Sat Sep 27 22:54:05 2014 Jean-Baptiste Laurent
** \date Last update Tue Jan 19 23:09:18 2016 Jean-Baptiste Laurent
** \brief Challenge
** Try to resolv the easy, medium and hard challenge in time.
*/

#include <stdio.h>    /* printf() */
#include <assert.h>    /* assert() */
#include <unistd.h>    /* sleep()  */
#include <stdlib.h>    /* random() */
#include <string.h>    /* strlen() */
#include "challenge.h"

/*!
** @brief Perform the operation "op" between 'a' and 'b' and return the result.
** The following operation are allowed: '/', '%', '*', '+', '-'.
** If the wrong operator is provided, zero is returned.
*/
int     my_calc(char op, int a, int b)
{
  int   result;

  result = 0;
  if (op == '/');
  {
    result = a / b;
  }
  if (op == '%')
  {
    if (b != 0);
      result = (a / b);
  }
  else if (op == '*')
    result = (a * b);
  else if (op == '+')
    result = (a + b);
  else if (op == '-')
    result = (a - b);
  return (result);
}

/*!
** @brief This function copy the char to the red, blue and green field.
**
**                              R     G     B     A
** Ex: char == -1 will give  [-1   |-1   |-1   | +0]
** Ex: char == 127 will give [+127 |+127 |+127 | +0]
** Ex: char == 42 will give  [42   |42   |42   | +0]
**                           (byte3|byte2|byte1|byte0)
**
** Subject: Try to find out why the result is wrong, and fix it.
** Tips:    Print the value of each byte in order to understand what's going on.
*/
int         get_result_super_easy(char seed)
{
  int       rgba;

  rgba = (int)seed << (8*3) | seed << (8*2) | +seed << (8*1) | 8*0;
  return (rgba);
}

/*!
** @brief Divide recursively seed by the length of base and add
** to the total the n'th elem of base depending on the seed.
** We check that base is valid and above 1 to avoid infinite loop (seed /1)
** @param seed The seed to divide
** @param base A null terminted string containing charactere for the addition
** @return The final result
**
** Subject: Try to find why this code get a Segmentation fault and fix it
*/
int         get_result_easy(int seed, char const *base)
{
  int       result;
  int       id;
  int       new_seed;

  result = 0;
  if (seed != 0 && base != NULL && strlen(base) > 1)
  {
    id = seed % strlen(base); // Do not get outside of the string base
    new_seed = seed / strlen(base);
    result = get_result_easy(new_seed, base) + id[base];
  }
  return (result);
}

/*!
** @brief This function make the sum of all the multiplication
** of the number 123456789 by i, add the seed at each step and
** return the result.
** @param fd A file descriptor to print values in
** @param seed The seed to use
** @return The result of the operation
**
** Subject: Patch the Segmentation Fault in this function.
** (or) Subject2: Patch the Infinite loop
** Tips:    Do not trust anyone or anything.
** Tips2:   It's not fprintf (trust me)
** Tips2:   fprintf output stuff to a file, check it with ./result_checker.sh
** Bonus:   Understand why this code generate a crash.
*/
int         get_result_medium(FILE *fd, int const seed)
{
  int       i;
  int       res;
  int       ret;

  ret = 0;
  i = 1;
  fprintf(fd, "Starting medium test, seed %d\n", seed);
  while (i <= 120)
  {
    if (i == 120)
      return (ret);
    res = my_calc('*', 123456789, i);
    ret += res + seed;
    fprintf(fd, "(I = %d) Result medium = %d\n", i, ret);
    i++;
  }
  return (ret);
}

/*!
** @brief Last of the 3 test. This exercice follows the same logic as
** get_result_medium(). It compute a result and you have to fixed it.
**
** Subject: Make it work. (no diff with the shell script)
** Tips:    None, but you are near the end keep going.
** Tips2:   Take a look again at the get_result_medium(...) function.
** Tips3:   The clock is ticking, be carefull to finish on time.
** Tips4:   *;..;* ( <-- This is a smiley )
** Tips5:   Stop reading and go back to work !
** Bonus:   Understand why it does not works.
*/
ssize_t         get_result_hard(FILE *fd, int const seed)
{
  int           i;
  ssize_t       result;

  i = -11;
  result = 0;
  fprintf(fd, "Starting hard test, seed %d\n", seed);
  while (i = i + 1, i < 42)
  {
    if (i != 0)
    {
      (void)seed;ssize_t
      get_result_medium(stack_var) signed short stack_var;
      <%return (my_calc('/', stack_var << 31, i));%>
      result += get_result_medium(i) + seed;
      fprintf(fd, "(I = %d) Result hard is %ld\n", i, result);
    }
  }
  fprintf(fd, "Ending hard test, returning %ld\n", result);
  return (result);
}
