/*!
** \file hidden_main.c
** 
** \author Jean-Baptiste Laurent
** \<jeanbaptiste.laurent.pro@gmail.com\>
** 
** \date Started on  Sat Sep 27 23:25:25 2014 Jean-Baptiste Laurent
** \date Last update Tue Sep  8 01:47:29 2015 Jean-Baptiste Laurent
** \brief File description to place here
*/

#include <stdio.h>	/* printf() */
#include <assert.h>	/* assert() */
#include <unistd.h>	/* sleep() */
#include <stdlib.h>	/* random() */
#include "challenge.h"

/*!
** @brief The correction of the easy exercice
*/
int	get_result_super_easy_corrected(char seed)
{
  unsigned char	useed;
  unsigned int	rgba;

  useed = seed;
  rgba = useed << 24 | useed << 16 | useed << 8 | 0;
  return ((int)rgba);
}

/*!
** @brief A second correction of the easy exercice
*/
int	get_result_super_easy_corrected2(char seed)
{
  union
  {
    struct
    {
      char	a;
      char	b;
      char	g;
      char	r;
    };
    int	rgba;
  }s;

  s.rgba = -1;
  s.rgba &= ~0xFF;
  s.r = seed;
  s.g = seed;
  s.b = seed;
  return (s.rgba);
}

void	check_super_easy_challenge()
{
  /* STEP 1 */
  int	uret;

  uret = get_result_super_easy(-1);assert(uret == get_result_super_easy_corrected(-1));
  uret = get_result_super_easy(127);assert(uret == get_result_super_easy_corrected(127));
  uret = get_result_super_easy(128);assert(uret == get_result_super_easy_corrected(128));
  uret = get_result_super_easy(-2);assert(uret == get_result_super_easy_corrected(-2));
  printf("Congratulation ! (nope, that was too easy)\n");
}

void	check_easy_challenge()
{
  int	ret;

  ret = get_result_easy(-10, "0123456789");
  /* printf("Result = %d\n", ret); */
  assert(ret == 48);
  ret = get_result_easy(-100, "0123456789");
  /* printf("Result = %d\n", ret); */
  assert(ret == 96);
  ret = get_result_easy(10, "0123456789");
  /* printf("Result = %d\n", ret); */
  assert(ret == 97);
  ret = get_result_easy(1337, "0123456789");
  /* printf("Result = %d\n", ret); */
  assert(ret == 206);
  ret = get_result_easy(42, "0123456789");
  /* printf("Result = %d\n", ret); */
  assert(ret == 102);
  printf("Congratulation ! Easy challenge success\n");
  printf("Starting next test ...\n");
}

void		check_medium_challenge(FILE *output_medium)
{
  ssize_t	ret;
  int		seed;

  /* STEP 2 */
  srandom(1);
  seed = random();
  { /* First time check */
    ret = get_result_medium(output_medium, seed);
    assert(ret == 975249557);
  }
  { /* Second time check */
    seed = random();ret = get_result_medium(output_medium, seed);
    if (ret != -1281261890)
      printf("You, YOU, filthy cheater !!!!!!! go kill yourself. (and have fun by the way)");
    assert(ret == -1281261890);
  }
  { /* Third time check */
    seed = random();
    ret = get_result_medium(output_medium, seed);
    assert(ret == -728844669);
  }
  printf("Congratulation ! (hum, not bad)\n");
}

void	check_hard_challenge(FILE *output_hard)
{
  ssize_t	ret;

  /* STEP 3 */
  ret = get_result_hard(output_hard, 123456789);
  /* printf("Ret = %ld\n", ret); */
  assert(ret == 462094561);
  fprintf(stderr, "Segmentation fault (core dumped)\n");
  sleep(5);
  printf("Just kidding.\n\n");
  printf("Congratulation ! You can now burn the world ! (or kitten)\n");
  printf("Have some rest, you deserve it\n");
}

/*!
** @brief The hidden main which is not provided by the challenge.
** It ensure that each exercice are well done.
*/
int		main()
{
  FILE		*output_medium;
  FILE		*output_hard;

  output_medium = fopen("./output_challenge_medium.txt", "w+");
  output_hard = fopen("./output_challenge_hard.txt", "w+");
  if (output_medium == NULL || output_hard == NULL)
  {
    perror("Unable to create challenge logs");
    return (EXIT_FAILURE);
  }

  check_super_easy_challenge();

  check_easy_challenge();

  check_medium_challenge(output_medium);
  fclose(output_medium);

  check_hard_challenge(output_hard);
  fclose(output_hard);

  return (EXIT_SUCCESS);
}
