/*
** The goal here is to patch the code while keeping in mind that
** all the algorithm are correctly written.
**
** You are allowed to add flags to the makefile if you need,
** and edit the source code. While rewritting the whole function
** can be tempting you wont be able to understand the source
** of the issue, which is why this challenge is made for.
**
** Use "#> ./result_checker" to validate your code.
**
** Exemple of exercice:
**
*/

/*!
** @brief This fonction does some black magic
** @param black_magic Look I am an int !
** @param random_stuff3 An other stuff that does not matter
*/
int	my_function(int black_magic, int random_stuff3)
{
  int	var1;
  int	*result;

  result = malloc(1); /* <-- Too small malloc */
  while (result != 0)
  {
    if ((random_stuff3 << 10) != 0)
      result[1] = (black_magic * 2) / random_stuff3 << 10; /* <-- result[1], out of bound error */
    random_stuff3 += 1;
  }
  return (0);
}

/*
**
** Here there are 2 mistakes:
** 1/ The malloc is wrong (correct ==> "result = malloc(sizeof(int *) * 1);")
** 2/ The malloc is not checked for NULL return
** 3/ We use "result[1]" instead of "result[0]";
** Keep in mind that the algorithm for the challenge are always OK,
** and you do not need to understand them to solve the exercice.
**
*/
